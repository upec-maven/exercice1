# exercice1

## Exercice 1
- Récuperez le projet à l'adresse https://gitlab.com/upec-maven/exercice1.git
- Vérifiez que la structure du projet ressemble à ceci:
![image.png](./image.png)
- Mettre à jour la version de java à JAVA 11
![image-1.png](./image-1.png)
- Compilez votre projet. Vous devez avoir une sortie similaire à :
![image-3.png](./image-3.png)
- Créez l'artifact JAR
- Lancez le programme pour s'assurer du bon fonctionnement à l'application (que constatez vous ?)
- Spécifiez le MANIFEST.MF
  ```
    <build>
        <plugins>
            <!-- all the existing build entries-->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.2.0</version>
                <configuration>
                    <archive>
                        <manifest>
                            <mainClass>edu.upec.m2.maven.App</mainClass>
                        </manifest>
                    </archive>
                </configuration>
            </plugin>
        </plugins>
    </build>  ```  

## Exercice 2 : Projet multimodules
- Créez un nouveau répertoire et copiez le projet précédent en deux  répertoires  : api et consumer
- Créez un pom parent
```
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>edu.upec.m2.maven</groupId>
  <artifactId>parent</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>edu.upec.m2.maven.parent</name>
  <!-- FIXME change it to the project's website -->
  <url>http://www.example.com</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>11</maven.compiler.source>
    <maven.compiler.target>11</maven.compiler.target>
  </properties>

  <modules>
    <module>api</module>
    <module>consumer</module>
  </modules>
</project> 
```

- Mettre à jour les poms individuels
Les artifactId doivent correspondent à ceux declarés dans le pom parent (soit 'api' et consumer)
- Vérifiez que le projet build bien

## Exercice 3 : Les dependences
- Dans votre module api, definissez l'interface suivante:
![image-4.png](./image-4.png)
- Définissez le module api comme dependence du module consumer
- Maintenant, modifiez la classe App du module consumer afin qu'elle ressemble à ceci : 
![image-5.png](./image-5.png)
- Buildez la projet
- Deployez les modules dans votre repository local
- BONUS : A l'aide de la documentation du plugin maven-assembly-plugin, créez un seul jar exécutable
